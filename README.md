# Data Incubator #

This is a collection of code I wrote to apply for the Data Incubator.  This code uses the [Twitch API](https://dev.twitch.tv/docs/v5/guides/using-the-twitch-api) to get a list of the top 1000 currently running streams, download basic stream statistics as well as the preview images, analyze the preview images for faces and text using [OpenCV](http://opencv.org/) in [Processing](https://processing.org/), and then look for factors that influence the viewership and followership by performing an analysis in [R](https://www.r-project.org/).

##Some interesting results:##
* The most important thing is a high quality stream.  Make sure you have a high resolution and high framerate.
* Onscreen chat seems to not matter at all---do what you like!
* Facecam doesn't seem to change viewership, but it significantly influences followership!  So if you want to bring more people into the stream, facecam will do nothing.  If you want to however convert more viewers into followers, adding the facecam seems to boost followership by almost 50%!