setwd("~/Documents/Processing 3 Sketches/TwitchGet2")
data <- read.csv("stats.csv", colClasses=c(ID = "numeric", VIEWERS = "numeric", NUM_FACES = "numeric", TOT_FACE_AREA = "numeric", AVE_FACE_X = "numeric", AVE_FACE_Y = "numeric", FOLLOWERS = "numeric", TOT_VIEWS = "numeric", HEIGHT = "numeric", FPS = "numeric", PARTNER = "numeric", TEXT_AREA = "numeric"), stringsAsFactors=FALSE)

e0 <- ecdf(data[which(data$NUM_FACES == 0),]$VIEWERS)
e1 <- ecdf(data[which(data$NUM_FACES != 0),]$VIEWERS)
l0 <- ecdf(log(data[which(data$NUM_FACES == 0),]$VIEWERS))
l1 <- ecdf(log(data[which(data$NUM_FACES != 0),]$VIEWERS))

plot(l0, verticals=TRUE, do.points=FALSE, col='blue')
plot(l1, verticals=TRUE, do.points=FALSE, col='orange', add=TRUE)

wilcox.test(data[which(data$NUM_FACES == 0),]$VIEWERS,data[which(data$NUM_FACES != 0),]$VIEWERS) #Not Significant

plot(log(data[which(data$NUM_FACES != 0),]$VIEWERS),log(data[which(data$NUM_FACES != 0),]$TOT_FACE_AREA)) 
cor.test(data[which(data$NUM_FACES != 0),]$TOT_FACE_AREA,data[which(data$NUM_FACES != 0),]$VIEWERS, method="spearman") #Not Significant

pairs(~log(VIEWERS)+log(TOT_FACE_AREA)+NUM_FACES+AVE_FACE_X+AVE_FACE_Y+FOLLOWERS+TOT_VIEWS+HEIGHT+FPS, data = data)
cor.test(data[which(data$NUM_FACES != 0),]$AVE_FACE_X,data[which(data$NUM_FACES != 0),]$VIEWERS, method="spearman") #Not Significant
cor.test(data[which(data$NUM_FACES != 0),]$AVE_FACE_Y,data[which(data$NUM_FACES != 0),]$VIEWERS, method="spearman") #Not Significant
cor.test(data$NUM_FACES,data$VIEWERS, method="spearman") #Not Significant

ks.test(data[which(data$NUM_FACES == 0),]$VIEWERS,data[which(data$NUM_FACES != 0),]$VIEWERS) #Not Significant
ks.test(data[which(data$NUM_FACES == 0),]$FOLLOWERS,data[which(data$NUM_FACES != 0),]$FOLLOWERS) #SIGNIFICANT

f0 <- ecdf(data[which(data$NUM_FACES == 0),]$FOLLOWERS)
f1 <- ecdf(data[which(data$NUM_FACES != 0),]$FOLLOWERS)
lf0 <- ecdf(log(data[which((data$NUM_FACES == 0)&(data$FOLLOWERS != 0)),]$FOLLOWERS))
lf1 <- ecdf(log(data[which((data$NUM_FACES != 0)&(data$FOLLOWERS != 0)),]$FOLLOWERS))

plot(lf0, verticals=TRUE, do.points=FALSE, col='blue')
plot(lf1, verticals=TRUE, do.points=FALSE, col='orange', add=TRUE)
wilcox.test(data[which(data$NUM_FACES == 0),]$FOLLOWERS,data[which(data$NUM_FACES != 0),]$FOLLOWERS) #Not Significant
ks.test(data[which(data$NUM_FACES == 0),]$FOLLOWERS,data[which(data$NUM_FACES != 0),]$FOLLOWERS,alternative = "greater") #p-value = 0.01969

cor.test(data[which(data$NUM_FACES != 0),]$TOT_FACE_AREA,data[which(data$NUM_FACES != 0),]$FOLLOWERS, method="spearman") #Not Significant

cor.test(data$HEIGHT,data$VIEWERS, method="spearman") #VERY SIGNIFICANT: p-value = 2.775e-07
plot(data$HEIGHT,data$VIEWERS)

cor.test(data$FPS,data$VIEWERS, method="spearman") #VERY SIGNIFICANT: p-value = 1.163e-07
plot(data$FPS,data$VIEWERS)

cor.test(data$HEIGHT,data$FOLLOWERS, method="spearman") #VERY SIGNIFICANT: p-value = 0.002252
plot(data$HEIGHT,data$FOLLOWERS)

cor.test(data$FPS,data$FOLLOWERS, method="spearman") #VERY SIGNIFICANT: p-value < 2.2e-16
plot(data$FPS,data$FOLLOWERS)

ks.test(data[which((data$FPS == 30) | (data$FPS == 60)),]$VIEWERS, data[which((data$FPS != 30) & (data$FPS != 60)),]$VIEWERS) #Not Significant

# Nicer graphs from http://minimaxir.com/2015/02/ggplot-tutorial/
# install.packages(c("ggplot2","RColorBrewer","scales"))
library(ggplot2); library(scales); library(grid); library(RColorBrewer)
fte_theme <- function() {
      
# Generate the colors for the chart procedurally with RColorBrewer
palette <- brewer.pal("Greys", n=9)
color.background = palette[2]
color.grid.major = palette[3]
color.axis.text = palette[6]
color.axis.title = palette[7]
color.title = palette[9]
      
# Begin construction of chart
theme_bw(base_size=9) +
        
# Set the entire chart region to a light gray color
theme(panel.background=element_rect(fill=color.background, color=color.background)) +
theme(plot.background=element_rect(fill=color.background, color=color.background)) +
theme(panel.border=element_rect(color=color.background)) +
      
# Format the grid
theme(panel.grid.major=element_line(color=color.grid.major,size=.25)) +
theme(panel.grid.minor=element_blank()) +
theme(axis.ticks=element_blank()) +
      
# Format the legend, but hide by default
#theme(legend.position="none") +
theme(legend.title=element_blank()) +
theme(legend.background = element_blank()) +
theme(legend.text = element_text(size=7,color=color.axis.title)) +
      
# Set title and axis labels, and format these and tick marks
theme(plot.title=element_text(color=color.title, size=10, vjust=1.25)) +
theme(axis.text.x=element_text(size=7,color=color.axis.text)) +
theme(axis.text.y=element_text(size=7,color=color.axis.text)) +
theme(axis.title.x=element_text(size=8,color=color.axis.title, vjust=0)) +
theme(axis.title.y=element_text(size=8,color=color.axis.title, vjust=1.25)) +
      
# Plot margins
theme(plot.margin = unit(c(0.35, 0.2, 0.3, 0.35), "cm"))
}

ggplot(data, aes(x=FPS, y=FOLLOWERS)) +
    geom_point(alpha=0.25,stroke=FALSE) +
    scale_y_log10(labels=comma) +
    fte_theme() +
    labs(x="Average Framerate", y="Number of Followers")
    
ggsave("graph1.png", dpi=600, width=4, height=3)

ggplot(data, aes(x=HEIGHT, y=FOLLOWERS)) +
    geom_point(alpha=0.25,stroke=FALSE) +
    scale_y_log10(labels=comma) +
    fte_theme() +
    labs(x="Video Height", y="Number of Followers")
    
ggsave("graph2.png", dpi=600, width=4, height=3)

ggplot(data, aes(x=FPS, y=VIEWERS)) +
    geom_point(alpha=0.25,stroke=FALSE) +
    scale_y_log10(labels=comma) +
    fte_theme() +
    labs(x="Average Framerate", y="Number of Viewers")
    
ggsave("graph3.png", dpi=600, width=4, height=3)

ggplot(data, aes(x=HEIGHT, y=VIEWERS)) +
    geom_point(alpha=0.25,stroke=FALSE) +
    scale_y_log10(labels=comma) +
    fte_theme() +
    labs(x="Video Height", y="Number of Viewers")
    
ggsave("graph4.png", dpi=600, width=4, height=3)

ggplot(data, aes(x=FOLLOWERS,color=(ifelse(data$NUM_FACES == 0,"Face","No Face")))) + 
	stat_ecdf(geom = "step") +
	scale_x_log10(labels=comma) + 
	fte_theme() +
	labs(x="Number of Followers",y="Cumulative Probability")
	
ggsave("graph5.png", dpi=600, width=4, height=3)

ggplot(data, aes(x=VIEWERS,color=(ifelse(data$NUM_FACES == 0,"Face","No Face")))) + 
	stat_ecdf(geom = "step") +
	scale_x_log10(labels=comma) + 
	fte_theme() +
	labs(x="Number of Viewers",y="Cumulative Probability")
	
ggsave("graph6.png", dpi=600, width=4, height=3)

cor.test(data$TEXT_AREA,data$FOLLOWERS, method="spearman")

ggplot(data, aes(x=TEXT_AREA, y=FOLLOWERS)) +
    geom_point(alpha=0.25,stroke=FALSE) +
    scale_y_log10(labels=comma) +
    scale_x_log10(labels=comma) +
    fte_theme() +
    labs(x="Total Text Area", y="Number of Followers")
    
ggsave("graph7.png", dpi=600, width=4, height=3)

cor.test(data$TEXT_AREA,data$VIEWERS, method="spearman")

ggplot(data, aes(x=TEXT_AREA, y=VIEWERS)) +
    geom_point(alpha=0.25,stroke=FALSE) +
    scale_y_log10(labels=comma) +
    scale_x_log10(labels=comma) +
    fte_theme() +
    labs(x="Total Text Area", y="Number of Viewers")
    
ggsave("graph8.png", dpi=600, width=4, height=3)

median(data[which(data$NUM_FACES > 0),]$FOLLOWERS)/median(data[which(data$NUM_FACES == 0),]$FOLLOWERS) # 1.513416

sum(data$NUM_FACE == 0)/sum(data$NUM_FACE >= 0) # 0.468