import gab.opencv.*;
import java.awt.Rectangle;

// First get the current data:
/*
curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=0' > out0.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=100' > out1.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=200' > out2.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=300' > out3.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=400' > out4.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=500' > out5.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=600' > out6.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=700' > out7.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=800' > out8.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=900' > out9.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1000' > out10.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1100' > out11.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1200' > out12.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1300' > out13.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1400' > out14.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1500' > out15.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1600' > out16.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1700' > out17.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1800' > out18.txt;curl -H 'Client-ID: uwaza3st0wo9s9gwfzl1tp76dd6s3p' 'https://api.twitch.tv/kraken/streams/?limit=100&offset=1900' > out19.txt;


*/

int[][] outArray = new int[2000][13];
int idx = 0;
for (int t = 0; t < 20; t++) {
  //int t = 0;
  JSONObject json = loadJSONObject("out" + t + ".txt");
  JSONArray streams = json.getJSONArray("streams");
  for (int i = 0; i < streams.size(); i++) {
    //int i = 0;
    PGraphics preview = createGraphics(640,360);
    PImage temp = loadImage(streams.getJSONObject(i).getJSONObject("preview").getString("large"));
    preview.beginDraw();
    preview.image(temp,0,0); 
    
    // find faces
    OpenCV opencv = new OpenCV(this, temp);
    opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
    Rectangle[] faces = opencv.detect();
    
    // find text
    opencv.loadImage(temp);
    //opencv.gray();
    //opencv.getSnapshot().save("testG.png");
    opencv.findCannyEdges(192,220);
    //opencv.findSobelEdges(1,0);
    //opencv.getSnapshot().save("testS.png");
    //opencv.threshold(128);
    //opencv.getSnapshot().save("testT.png");
    opencv.dilate();
    opencv.dilate();
    opencv.erode();
    opencv.erode();
    PImage edges = opencv.getSnapshot();
    //edges.save("testC.png");
    
    ArrayList<Contour> contours = opencv.findContours();
    
    int totalTextAndUiArea = 0;
    for (Contour contour : contours) {
      preview.stroke(255,0,0);
      preview.noFill();
      Rectangle bb = contour.getBoundingBox();
      if ((bb.width*bb.height > 128) && (bb.width > 32)) {
        int count = 0;
        for (int di = 0; di < bb.width; di++) {
          for (int dj = 0; dj < bb.height; dj++) {
            count += brightness(edges.pixels[(bb.x+di)+(bb.y+dj)*edges.width])>128?1:0;
          }
        }
        if (count > 0.4*bb.width*bb.height) {
          preview.rect(bb.x,bb.y,bb.width,bb.height);
          totalTextAndUiArea += bb.width*bb.height;
        }
      }
    }
    
  
    preview.stroke(0,255,0);
    preview.noFill();
    int faceVol = 0;
    int ax = 0;
    int ay = 0;
    for (int j = 0; j < faces.length; j++) {
      faceVol += faces[j].width*faces[j].height;
      ax += faces[j].x*faceVol;
      ay += faces[j].y*faceVol;
      preview.rect(faces[j].x, faces[j].y, faces[j].width, faces[j].height);
    }
    if (faceVol > 0) {
      ax /= faceVol;
      ay /= faceVol;
    }
    preview.endDraw();
    outArray[idx][0] = streams.getJSONObject(i).getInt("_id");
    outArray[idx][1] = streams.getJSONObject(i).getInt("viewers");
    outArray[idx][2] = faces.length;
    outArray[idx][3] = faceVol;
    outArray[idx][4] = ax;
    outArray[idx][5] = ay;
    outArray[idx][6] = streams.getJSONObject(i).getJSONObject("channel").getInt("followers");
    outArray[idx][7] = streams.getJSONObject(i).getJSONObject("channel").getInt("views");
    outArray[idx][8] = streams.getJSONObject(i).getInt("video_height");
    outArray[idx][9] = int(streams.getJSONObject(i).getFloat("average_fps")); // just for now....
    outArray[idx][10] = streams.getJSONObject(i).getInt("delay");
    outArray[idx][11] = streams.getJSONObject(i).getJSONObject("channel").getBoolean("partner")?1:0;
    outArray[idx][12] = totalTextAndUiArea;
    preview.save("outDir/" + outArray[idx][0] + ".png");
    idx++;
  }
}

PrintWriter output = createWriter("stats.csv");
output.println("ID, VIEWERS, NUM_FACES, TOT_FACE_AREA, AVE_FACE_X, AVE_FACE_Y, FOLLOWERS, TOT_VIEWS, HEIGHT, FPS, DELAY, PARTNER, TEXT_AREA");
for (int i = 0; i < 1000; i++) {
  output.println(outArray[i][0] + ", " + outArray[i][1] + ", " + outArray[i][2] + ", " + outArray[i][3] + ", " + outArray[i][4] + ", " + outArray[i][5] + ", " + outArray[i][6] + ", " + outArray[i][7] + ", " + outArray[i][8] + ", " + outArray[i][9] + ", " + outArray[i][10] + ", " + outArray[i][11] + ", " + outArray[i][12]);
}
output.flush();
output.close();

exit();